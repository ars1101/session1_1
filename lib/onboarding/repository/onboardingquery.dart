import 'package:flutter/material.dart';

final List<Map<String,String>> originData = [
  {
    'title' : 'Quick Delivery At Your Doorstep',
    'subtitle' : 'Enjoy quick pick-up and delivery to your destination',
    'image' : 'assets/first.png'
  },
  {
    'title' : 'Flexible Payment',
    'subtitle' : 'Different modes of payment either before and after delivery without stress',
    'image' : 'assets/second.png'
  },
  {
    'title' : 'Real-time Tracking',
    'subtitle' : 'Track your packages/items from the comfort of your home till final destination',
    'image' : 'assets/third.png'
  }
];


