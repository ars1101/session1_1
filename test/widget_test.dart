// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.



import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:session1_1/onboarding/data/queue.dart';
import 'package:session1_1/onboarding/page/onboarding.dart';
import 'package:session1_1/onboarding/repository/onboardingquery.dart';

import 'package:session1_1/main.dart';

Queue queue = Queue();

void main() {



  testWidgets('Изображение и текста из очереди извлекается правильно (в порядке добавления в очередь).', (WidgetTester tester) async {
    queue.resetQueue();
    for(Map<String,String> value in originData){
      expect(value, queue.next());
    }
  });

  testWidgets('Корректное извлечение элементов из очереди (количество элементов в очереди уменьшается на единицу).', (WidgetTester tester) async {
    queue.resetQueue();
    int originlen = queue.length();
    queue.next();
    expect(queue.length(), originlen - 1);
  });

  testWidgets('В случае, когда в очереди несколько картинок, устанавливается правильная надпись на кнопке', (WidgetTester tester) async {
    queue.resetQueue();
    for(int i = 0; i < queue.length(); i = i+1){
      if(queue.length() > 1){
        expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget  );
      }else{
        expect(find.widgetWithText(FilledButton, "Next"), findsNothing);
        expect(find.widgetWithText(FilledButton, "Sign Up"), findsOneWidget);
      }
      queue.next();
    };
  });

  testWidgets('Случай, когда очередь пустая, надпись на кнопке должна измениться на "Sing Up".', (WidgetTester tester) async {
    queue.resetQueue();

      for (int i = 0; i <= queue.length(); i = i + 1) {
        await tester.pumpAndSettle();
        if(queue.length() == 1){
        expect(find.widgetWithText(FilledButton, "Sign Up"), findsOneWidget);}
      }

    queue.next();

  });

  testWidgets('Если очередь пустая и пользователь нажал на кнопку “Sing in”, происходит открытие пустого экрана «Holder» приложения. Если очередь не пустая – переход отсутствует.', (WidgetTester tester) async {
    queue.resetQueue();
    final pageView = find.byType(PageView);
    await tester.runAsync(() => tester.pumpWidget(const MaterialApp(
      home: onBoarding(),
    )));
    for (int i = 0; i <= queue.length(); i = i + 1) {
    if(queue.length() == 1) {
      await tester.pumpAndSettle();
      await tester.tap(find.widgetWithText(FilledButton, "Sign Up"));
      expect(find.byElementType(Holder), findsOneWidget);

    }
    queue.next();
    }
  });
}

